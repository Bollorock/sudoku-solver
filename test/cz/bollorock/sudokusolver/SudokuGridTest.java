/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver;

import cz.bollorock.sudokusolver.io.SudokuIO;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import junit.framework.TestCase;

/**
 *
 * @author Ladislav
 */
public class SudokuGridTest extends TestCase {

    private static final File SUDOKUS = new File("Sudokus");
    private static final SudokuIO SUDOKU_IO = new SudokuIO();
    private static final long TIMEOUT = 0;
    private static final int ENOUGH_SOLUTIONS = 0;

    public SudokuGridTest(String testName) {
        super(testName);
    }

    /**
     * Test of backtrackSolve method, of class SudokuGrid.
     */
    public void testBacktrackSolve() throws InterruptedException, ExecutionException {
        System.out.println("backtrackSolve");
        System.out.printf("Solve Test (timeout=%d; enoughSolutions=%d)\n", TIMEOUT, ENOUGH_SOLUTIONS);

        long sumLogic = 0;
        long sumBacktrack = 0;
        long sumFailed = 0;
        int countLogic = 0;
        int countBacktrack = 0;
        int countFailedToSolve = 0;
        HashMap<File, SudokuGrid> sudokus = readSudokus();
        for (Map.Entry<File, SudokuGrid> entry : sudokus.entrySet()) {
            long t = System.currentTimeMillis();
            SolverResponse backtrackSolve = entry.getValue().backtrackSolve(TIMEOUT, ENOUGH_SOLUTIONS);
            long x = System.currentTimeMillis() - t;
            System.out.printf("%30s %s\n", entry.getKey().toString(),backtrackSolve.toString());
            switch (backtrackSolve.getSudokuState()) {
                case SOLVED:
                    sumLogic += backtrackSolve.isLogicSolve() ? x : 0;
                    countLogic += backtrackSolve.isLogicSolve() ? 1 : 0;
                    sumBacktrack += !backtrackSolve.isLogicSolve() ? x : 0;
                    countBacktrack += !backtrackSolve.isLogicSolve() ? 1 : 0;
                    break;
                default:
                    sumFailed += x;
                    countFailedToSolve++;
            }
        }
        System.out.println(String.format("Tested: Total:%4d   Logic:%4d   Backtrack:%4d   FailedToSolve:%4d", countBacktrack + countFailedToSolve + countLogic, countLogic, countBacktrack, countFailedToSolve));
        System.out.println(String.format("Solve times: Total:%s  Logic:%s  Backtrack:%s  Failed:%s", getTime((sumBacktrack + sumLogic + sumFailed) / (countBacktrack + countFailedToSolve + countLogic)),
                countLogic == 0 ? " none" : getTime((sumLogic) / (countLogic)), countBacktrack == 0 ? " none" : getTime((sumBacktrack) / (countBacktrack)), countFailedToSolve == 0 ? " none" : getTime((sumFailed) / (countFailedToSolve))));
        assertEquals(0, countFailedToSolve);
    }

    private String getTime(long t) {
        long x = t;
        long m = x / (60 * 1000);
        x -= m * (60 * 1000);
        long s = x / 1000;
        x -= s * 1000;
//        return String.format("%2dm %2ds %3dms", m, s, x);
        return String.format("%3dms", t);
    }

    private HashMap<File, SudokuGrid> readSudokus() {
        HashMap<File, SudokuGrid> sudokus = new HashMap<>();
        LinkedList<File> files = new LinkedList<>();
        Collections.addAll(files, SUDOKUS.listFiles());
        while (!files.isEmpty()) {
            File f = files.poll();
            if (f.isDirectory()) {
                Collections.addAll(files, f.listFiles());
            } else {
                SudokuGrid sudoku = null;
                try {
                    sudoku = SUDOKU_IO.readSudoku(f);
                } catch (IOException ex) {
                }
                if (sudoku != null) {
                    sudokus.put(f, sudoku);
                }
            }
        }
        return sudokus;
    }
}
