/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver;

/**
 *
 * @author Ladislav
 */
public class SolverResponse {

    private final StopReason stopReason;
    private final SudokuState sudokuState;
    private final boolean logicSolve;
    private final int numberOfSolutions;
    private final String desc;

    /**
     *
     * @param stopReason
     * @param sudokuState
     * @param numberOfSolutions
     */
    public SolverResponse(StopReason stopReason, SudokuState sudokuState, boolean logicSolve, int numberOfSolutions, String desc) {
        this.stopReason = stopReason;
        this.sudokuState = sudokuState;
        this.logicSolve = logicSolve;
        this.numberOfSolutions = numberOfSolutions;
        this.desc = desc;
    }

    public StopReason getStopReason() {
        return stopReason;
    }

    public SudokuState getSudokuState() {
        return sudokuState;
    }

    public int getNumberOfSolutions() {
        return numberOfSolutions;
    }

    public boolean isLogicSolve() {
        return logicSolve;
    }

    public String getDesc() {
        return desc;
    }    

    @Override
    public String toString() {
        return String.format("%11s %15s %9s %3d %s", this.getSudokuState(), this.getStopReason(), this.isLogicSolve() ? "LOGIC" : "BACKTRACK", this.getNumberOfSolutions(), this.getDesc());
    }
}
