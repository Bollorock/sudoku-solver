/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.gui;

import cz.bollorock.sudokusolver.*;
import cz.bollorock.sudokusolver.io.SudokuIO;
import cz.bollorock.sudokusolver.solvers.RegisteredSolvers;
import cz.bollorock.sudokusolver.solvers.Solver;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;

/**
 *
 * @author Ladislav
 */
public class SudokuGUI {
    
    private static final Logger LOG = Logger.getLogger(SudokuGUI.class.getName());
    ;
    private SudokuGrid grid;
    private SudokuIO sudokuIO;
    private GridComponent gridComponent;
    private JSpinner timeOut;
    private LinkedList<SolverButton> buttons;
    private final JSpinner enoughSolutions;
    private final JLabel status;
    private final JFrame frame;
    private final JButton stopBtn;
    private final JButton logicSolveBtn;
    private final JButton backtrackSolveBtn;
    private volatile boolean solving;
    
    public SudokuGUI() throws HeadlessException {
        frame = new JFrame("Sudoku Solver");
        frame.setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buttons = new LinkedList<>();
        grid = new SudokuGrid();
        sudokuIO = new SudokuIO();
        
        status = new JLabel("Status");
        frame.add(status, BorderLayout.NORTH);
        
        gridComponent = new GridComponent(grid);
        frame.add(gridComponent, BorderLayout.CENTER);

        //<editor-fold defaultstate="collapsed" desc="MenuBar">
        JMenuBar bar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.add(new JMenuItem(new AbstractAction("Open Sudoku") {
            @Override
            public void actionPerformed(ActionEvent e) {
                grid.clear();
                try {
                    SudokuGrid newSudoku = sudokuIO.openSudoku(frame);
                    if (newSudoku != null) {
                        grid.setValues(newSudoku, true);
                    }
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(frame, ex, "Error!", JOptionPane.ERROR_MESSAGE);
//                    LOG.catching(Level.ERROR, ex);
                }
                gridComponent.repaint();
            }
        }));
        fileMenu.add(new JMenuItem(new AbstractAction("Save Sudoku") {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sudokuIO.saveSudoku(frame, grid);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(frame, ex, "Error!", JOptionPane.ERROR_MESSAGE);
//                    LOG.catching(Level.ERROR, ex);
                }
            }
        }));
        fileMenu.add(new JMenuItem(new AbstractAction("Import Sudoku") {
            @Override
            public void actionPerformed(ActionEvent e) {
                String newSudoku = JOptionPane.showInputDialog(frame, "Enter new sudoku:");
                if (newSudoku != null && newSudoku.matches("[0-9,._]{81}")) {
                    grid.setValues(newSudoku);
                    gridComponent.repaint();
                }
            }
        }));
        fileMenu.add(new JMenuItem(new AbstractAction("Export Sudoku") {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showInputDialog(frame, "Exported sudoku:", grid.toLineString());
            }
        }));
        fileMenu.add(new JMenuItem(new AbstractAction("Clear Sudoku") {
            @Override
            public void actionPerformed(ActionEvent e) {
                grid.clear();
                gridComponent.repaint();
            }
        }));
        fileMenu.add(new JMenuItem(new AbstractAction("Reset Sudoku") {
            @Override
            public void actionPerformed(ActionEvent e) {
                grid.reset();
                gridComponent.repaint();
            }
        }));
        JCheckBoxMenuItem item = new JCheckBoxMenuItem(new AbstractAction("Show Posible Values") {
            @Override
            public void actionPerformed(ActionEvent e) {
                gridComponent.setShowTips(((JCheckBoxMenuItem) e.getSource()).isSelected());
                gridComponent.repaint();
            }
        });
        item.setSelected(gridComponent.isShowTips());
        fileMenu.add(item);
        
        bar.add(fileMenu);
        frame.setJMenuBar(bar);
        //</editor-fold>
        
        JPanel panel = new JPanel(new FlowLayout());
        
        //<editor-fold defaultstate="collapsed" desc="Solve Parameters Panel">
        timeOut = new JSpinner(new SpinnerNumberModel(1000, 0, Integer.MAX_VALUE, 1000));
        enoughSolutions = new JSpinner(new SpinnerNumberModel(1, 0, Integer.MAX_VALUE, 1));
        panel.add(timeOut);
        panel.add(enoughSolutions);
        logicSolveBtn = new JButton(new AbstractAction("Logic Solve") {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSolving(true);
                status.setText("Solving...");
                new SwingWorker<SolverResponse, Object>() {
                    @Override
                    protected SolverResponse doInBackground() throws Exception {
                        return grid.logicSolve();
                    }
                    
                    @Override
                    protected void done() {
                        setSolving(false);
                        gridComponent.repaint();
                        try {
                            status.setText(get().toString());
                        } catch (InterruptedException | ExecutionException ex) {
                            LOG.log(Level.WARNING, "", ex);
                        }
                    }
                }.execute();
            }
        });
        panel.add(logicSolveBtn);
        
        backtrackSolveBtn = new JButton(new AbstractAction("Backtrack Solve") {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSolving(true);
                status.setText("Solving...");
                new SwingWorker<SolverResponse, Object>() {
                    @Override
                    protected SolverResponse doInBackground() throws Exception {
                        return grid.backtrackSolve((Integer) timeOut.getValue(), (Integer) enoughSolutions.getValue());
                    }
                    
                    @Override
                    protected void done() {
                        setSolving(false);
                        gridComponent.repaint();
                        try {
                            status.setText(get().toString());
                        } catch (InterruptedException | ExecutionException ex) {
                            LOG.log(Level.WARNING, "", ex);
                        }
                    }
                }.execute();
            }
        });
        panel.add(backtrackSolveBtn);
        
        stopBtn = new JButton(new AbstractAction("Stop") {
            @Override
            public void actionPerformed(ActionEvent e) {
                grid.stopSolving();
            }
        });
        stopBtn.setEnabled(false);
        panel.add(stopBtn);
        frame.add(panel, BorderLayout.SOUTH);
        //</editor-fold>
                
        //<editor-fold defaultstate="collapsed" desc="Solvers Panel">
        panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 5, 5);
        c.gridx = 0;
        c.gridy = 0;
        Iterator<Entry<Solver, Boolean>> it = RegisteredSolvers.getSolvers().entrySet().iterator();
        while (it.hasNext()) {
            Entry<Solver, Boolean> entry = it.next();
            buttons.add(new SolverButton(entry.getKey(), new AbstractAction(entry.getKey().getName()) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SolverButton sb = (SolverButton) e.getSource();
                    sb.applySolver(grid);
                    gridComponent.repaint();
                }
            }, this));
            buttons.getLast().setEnabled(entry.getValue());
            panel.add(buttons.getLast(), c);
            c.gridy++;
        }
        frame.add(panel, BorderLayout.EAST);
        //</editor-fold>
        
        frame.pack();
    }
    
    /**
     * Updates buttons enabled flags
     */
    public void updateButtons() {
        for (SolverButton btn : buttons) {
            btn.setEnabled(!solving && RegisteredSolvers.isEnabled(btn.getSolver()));
        }
        stopBtn.setEnabled(solving);
        logicSolveBtn.setEnabled(!solving);
        backtrackSolveBtn.setEnabled(!solving);
        gridComponent.setEnabled(!solving);
    }
    
    private void setSolving(boolean solving) {
        this.solving = solving;
        updateButtons();
    }
    
    public void setVisible(boolean visible) {
        frame.setVisible(visible);
    }
}
