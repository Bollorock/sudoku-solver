/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.gui;

import cz.bollorock.sudokusolver.SudokuGrid;
import cz.bollorock.sudokusolver.solvers.RegisteredSolvers;
import cz.bollorock.sudokusolver.solvers.Solver;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.Timer;
import javax.swing.plaf.ColorUIResource;

/**
 *
 * @author Ladislav
 */
public class SolverButton extends JButton {    
    private static final Color BACKGROUND = new ColorUIResource(214,217,223);
    private static final int DELAY = 800;
    
    private SudokuGUI gui;
    private Solver solver;
    private final Timer timer;

    
    public SolverButton(final Solver solver, Action action, SudokuGUI sudokuGUI) {
        super(action);
        timer = new Timer(DELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetBackground();
            }
        });
        timer.setInitialDelay(DELAY);
        timer.setRepeats(false);
        this.solver = solver;
        this.gui = sudokuGUI;
        
        //<editor-fold defaultstate="collapsed" desc="MouseListener">
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == 3 && (isEnabled() || !RegisteredSolvers.isEnabled(solver))) {
                    RegisteredSolvers.setEnabled(solver, !RegisteredSolvers.isEnabled(solver));
                    gui.updateButtons();
                }
            }
        });
        //</editor-fold>
    }

    public Solver getSolver() {
        return solver;
    }

    public void setSolver(Solver solver) {
        this.solver = solver;
        this.setText(solver.getName());
    }

    public boolean applySolver(SudokuGrid sudoku) {
        if (RegisteredSolvers.isEnabled(solver)) {
            boolean res = solver.applySolver(sudoku);
            if (res) {
                this.setBackground(Color.green);
            } else {
                this.setBackground(Color.red);
            }
            if (timer != null) {
                timer.restart();
            }
            return res;
        }
        return false;
    }
    
    private void resetBackground(){
        setBackground(BACKGROUND);
    }
}
