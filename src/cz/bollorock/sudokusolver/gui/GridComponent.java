/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.gui;

import cz.bollorock.sudokusolver.SudokuGrid;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import javax.swing.JComponent;
        

/**
 *
 * @author Ladislav
 */
public class GridComponent extends JComponent {

    private static final Font SMALL = new Font(Font.MONOSPACED, 0, 11);
    private static final Font BIG = new Font(Font.MONOSPACED, Font.BOLD, 28);
    private static final Color FG = Color.BLACK;
    private static final Color HIGHLIGHT = new Color(51, 181, 229);
    private SudokuGrid grid;
    private Cell selected;
    private boolean showTips = true;
    private static final HashMap<Character, Integer> numbers;

    static {
        numbers = new HashMap<>();
        numbers.put('+', 1);
        numbers.put('ě', 2);
        numbers.put('š', 3);
        numbers.put('č', 4);
        numbers.put('ř', 5);
        numbers.put('ž', 6);
        numbers.put('ý', 7);
        numbers.put('á', 8);
        numbers.put('í', 9);
        numbers.put('é', 0);
        numbers.put('1', 1);
        numbers.put('2', 2);
        numbers.put('3', 3);
        numbers.put('4', 4);
        numbers.put('5', 5);
        numbers.put('6', 6);
        numbers.put('7', 7);
        numbers.put('8', 8);
        numbers.put('9', 9);
        numbers.put('0', 0);
    }

    public GridComponent(final SudokuGrid grid) {
        this.setFocusable(true);
        this.grid = grid;
        this.setLayout(new GridLayout(9, 9));
        
        //<editor-fold defaultstate="collapsed" desc="MousListener">
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (!isEnabled()) {
                    return;
                }
                requestFocus();
                Cell cell = getCell(e.getPoint());
                if (cell.equals(selected)) {
                    selected = null;
                } else {
                    selected = cell;
                }
                repaint();
            }
        });
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="KeyListener">
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                Integer i = numbers.get(e.getKeyChar());
                if (isEnabled() && i != null && selected != null && !grid.isInitial(selected.getRow(), selected.getCol()) && i >= 0 && i <= 9) {
                    if (e.isAltDown()) {
                        grid.setPossibleValue(selected.getRow(), selected.getCol(), i, false);
                    } else {
                        grid.setValue(selected.getRow(), selected.getCol(), i);
                    }
                    repaint();
                }
            }
            
            @Override
            public void keyPressed(KeyEvent e) {
                if (selected != null) {
                    switch (e.getKeyCode()) {
                        case 37:
                            selected.setCol(selected.getCol() > 0 ? selected.getCol() - 1 : selected.getCol());
                            break;
                        case 38:
                            selected.setRow(selected.getRow() > 0 ? selected.getRow() - 1 : selected.getRow());
                            break;
                        case 39:
                            selected.setCol(selected.getCol() < 8 ? selected.getCol() + 1 : selected.getCol());
                            break;
                        case 40:
                            selected.setRow(selected.getRow() < 8 ? selected.getRow() + 1 : selected.getRow());
                            break;
                        default:
                    }
                    repaint();
                }
            }
        });
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="FocusListener">
        this.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                if (selected != null) {
                    selected = null;
                    updateUI();
                }
            }
        });
        //</editor-fold>
    }

    private Cell getCell(Point point) {
        return new Cell((int) (point.y * 9d / getHeight()), (int) (point.x * 9d / getWidth()));
    }

    @Override
    public void paint(Graphics g) {
        double w = this.getWidth() / 9d;
        double h = this.getHeight() / 9d;
        for (int r = 0; r < SudokuGrid.SIZE; r++) {
            for (int c = 0; c < SudokuGrid.SIZE; c++) {
                paintField(r, c, w, h, g);
            }
        }
        g.setColor(FG);
        for (int i = 0; i < 10; i++) {
            horizontalLine(g, (int) (i * h), i % 3 == 0 ? 3 : 1);
            verticalLine(g, (int) (i * w), i % 3 == 0 ? 3 : 1);
        }
    }

    private void paintField(int row, int col, double width, double height, Graphics g) {
        double baseX = (int) (col * width);
        double baseY = (int) (row * height);
        if (selected != null && selected.getCol() == col && selected.getRow() == row) {
            g.setColor(HIGHLIGHT);
            g.fillRect((int) (baseX), (int) (baseY), (int) (width), (int) (height));
        }
        if (grid.getValue(row, col) == 0 && showTips) {
            boolean[] posibleValues = grid.getPossibleValues(row, col);
            g.setFont(SMALL);
            g.setColor(FG);
            double w = width / SudokuGrid.SMALL_SIZE;
            double h = height / SudokuGrid.SMALL_SIZE;
            double bx = (w - g.getFontMetrics().stringWidth("8")) / 2d + baseX;
            double by = (h + g.getFontMetrics().getHeight() - 1) / 2d + baseY;
            for (int i = 0; i < posibleValues.length; i++) {
                if (posibleValues[i]) {
                    int x = (int) (bx + (i % 3) * w);
                    int y = (int) (by + (i / 3) * h);
                    g.drawString("" + (i + 1), x, y);
                }
            }
        } else if (grid.getValue(row, col) != 0) {
            if (grid.isInitial(row, col)) {
                g.setColor(Color.RED);
            } else {
                g.setColor(FG);
            }
            g.setFont(BIG);
            String value = grid.getValue(row, col) + "";
            double x = (width - g.getFontMetrics().stringWidth(value)) / 2d;
            double y = (height + g.getFontMetrics().getHeight()) / 2d;
            g.drawString(value, (int) (baseX + x), (int) (baseY + y));
        }
    }

    @Override
    public Dimension getPreferredSize() {
        int w = Math.max(getFontMetrics(BIG).stringWidth("8"), getFontMetrics(SMALL).stringWidth("8") * 3) * 9;
        int h = Math.max(getFontMetrics(BIG).getHeight(), getFontMetrics(SMALL).getHeight() * 3) * 9;
        int i = Math.max(w, h);
        return new Dimension(i, i);
    }

    private void horizontalLine(Graphics g, int y, int width) {
        for (int i = -width / 2; i < width / 2 + 1; i++) {
            g.drawLine(0, y + i, this.getWidth(), y + i);
        }
    }

    private void verticalLine(Graphics g, int x, int width) {
        for (int i = -width / 2; i < width / 2 + 1; i++) {
            g.drawLine(x + i, 0, x + i, this.getHeight());
        }
    }

    public void setShowTips(boolean showTips) {
        this.showTips = showTips;
    }

    public boolean isShowTips() {
        return showTips;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (!enabled) {
            selected = null;
            repaint();
        }
    }
    
    
}
