/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.solvers;

import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ladislav
 */
public class RegisteredSolvers {

    private static final Logger LOG = Logger.getLogger(RegisteredSolvers.class.getName());
    
    private static final TreeMap<Solver, Boolean> solvers;

    static {
        solvers = new TreeMap<>();
        registerSolver(new SolvedCells());
        registerSolver(new PossibleValues());
        registerSolver(new HiddenSingles());
        registerSolver(new NakedPairs());
        registerSolver(new PointingPairsTriples());
        registerSolver(new BoxLineReduction());
        
        registerSolver(new XWing());
    }

    public static SortedMap<Solver, Boolean> getSolvers() {
        return Collections.unmodifiableSortedMap(solvers);
    }

    public static boolean isEnabled(Solver solver) {
        return solvers.get(solver);
    }

    public static void setEnabled(Solver solver, boolean enabled) {
        if (solvers.containsKey(solver)) {
            solvers.put(solver, enabled);
        }
    }

    public static void registerSolver(Solver solver) {
        LOG.log(Level.FINE, String.format("Registering %s (%d)", solver.getName(), solver.getComplexity()));
        if (solvers.containsKey(solver)) {
            LOG.log(Level.SEVERE, solver.toString() + " is allready registered.");
        }
        solvers.put(solver, true);
    }

    public static void unregisterSolver(Solver solver) {
        LOG.log(Level.FINE, String.format("Unregistering %s (%d)", solver.getName(), solver.getComplexity()));
        solvers.remove(solver);
    }
}
