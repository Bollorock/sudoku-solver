/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.solvers;

import cz.bollorock.sudokusolver.SudokuGrid;

/**
 *
 * @author Ladislav
 */
public class XWing extends Solver {

    public static final String NAME = "X-Wing";
    public static final int COMPLEXITY = 7;

    public XWing() {
        super(NAME, COMPLEXITY);
    }


    @Override
    public boolean applySolver(SudokuGrid sudoku) {
        return searchInRows() | searchInCols();
    }

    private boolean searchInRows() {
        int[][] cols = new int[SudokuGrid.SIZE][2];

        for (int r = 0; r < SudokuGrid.SIZE; r++) {
            for (int c = 0; c < SudokuGrid.SIZE; c++) {
                
            }
        }

        return false;
    }

    private boolean searchInCols() {



        return false;
    }
}
