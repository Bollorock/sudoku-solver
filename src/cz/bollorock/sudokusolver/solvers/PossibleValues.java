/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.solvers;

import cz.bollorock.sudokusolver.SudokuGrid;

/**
 *
 * @author Ladislav
 */
public class PossibleValues extends Solver{
    public static final String NAME = "Possible Values";
    public static final int COMPLEXITY = 1;

    public PossibleValues() {
        super(NAME, COMPLEXITY);
    }

    @Override
    public boolean applySolver(SudokuGrid sudoku) {
        boolean valueChanged = false;
        for (int i = 0; i < 9; i++) {
            valueChanged |= updatePossibleValuesRow(sudoku, i);
            valueChanged |= updatePossibleValuesColumn(sudoku, i);
            valueChanged |= updatePossibleValuesSquare(sudoku, i / 3, i % 3);
        }
        return valueChanged;
    }
    
    /**
     * Updates posible values in row.
     *
     * @param row Zero-based index of row, top to bottom.
     * @return true if any field was modified
     */
    private boolean updatePossibleValuesRow(SudokuGrid s, int row) {
        boolean[] used = new boolean[SudokuGrid.SIZE];
        int value;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            value = s.getValue(row, i);
            if (value > 0 && value <= SudokuGrid.SIZE) {
                used[value - 1] = true;
            }
        }
        boolean valueChanged = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            valueChanged = valueChanged | s.removePosibleValues(row, i, used);
        }
        return valueChanged;
    }

    /**
     * Updates posible values in column.
     *
     * @param col Zero-based column index, left to right.
     * @return true if any field was modified
     */
    private boolean updatePossibleValuesColumn(SudokuGrid s, int col) {
        boolean[] used = new boolean[SudokuGrid.SIZE];
        int value;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            value = s.getValue(i, col);
            if (value > 0 && value <= SudokuGrid.SIZE) {
                used[value - 1] = true;
            }
        }
        boolean valueChanged = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            valueChanged = valueChanged | s.removePosibleValues(i, col, used);
        }
        return valueChanged;
    }

    /**
     * Updates posible values in small square.
     *
     * @param row Zero-based row index of small square, top to bottom.
     * @param col Zero-based column index of small square, left to right.
     * @return true if any field was modified
     */
    private boolean updatePossibleValuesSquare(SudokuGrid s, int row, int col) {
        boolean[] used = new boolean[SudokuGrid.SIZE];
        int value;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            value = s.getValue(row * 3 + i / 3, col * 3 + i % 3);
            if (value > 0 && value <= SudokuGrid.SIZE) {
                used[value - 1] = true;
            }
        }
        boolean valueChanged = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            valueChanged = valueChanged | s.removePosibleValues(row * 3 + i / 3, col * 3 + i % 3, used);
        }
        return valueChanged;
    }
    
}
