/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.solvers;

import cz.bollorock.sudokusolver.SudokuGrid;

/**
 *
 * @author Ladislav
 */
public class HiddenSingles extends Solver {

    public static final String NAME = "Hidden Singles";
    public static final int COMPLEXITY = 2;

    public HiddenSingles() {
        super(NAME, COMPLEXITY);
    }
    
    @Override
    public boolean applySolver(SudokuGrid sudoku) {
        boolean valueChanged = false;
        for (int i = 0; i < 9; i++) {
            valueChanged |= updateSingletonRow(sudoku, i);
            valueChanged |= updateSingletonColumn(sudoku, i);
            valueChanged |= updateSingletonSquare(sudoku, i / 3, i % 3);
        }
        return valueChanged;
    }

    //<editor-fold defaultstate="collapsed" desc="updateSingletons">
    private boolean updateSingletonRow(SudokuGrid sudoku, int row) {
        int[][] d = new int[9][2];
        boolean[] posibleValues;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            posibleValues = sudoku.getPossibleValues(row, i);
            for (int j = 0; j < posibleValues.length; j++) {
                if (posibleValues[j]) {
                    d[j][0]++;
                    d[j][1] = i;
                }
            }
        }
        boolean changed = false;
        for (int i = 0; i < d.length; i++) {
            if (d[i][0] == 1) {
                for (int j = 0; j < SudokuGrid.SIZE; j++) {
                    if (j != i) {
                        changed |= sudoku.setPossibleValue(row, d[i][1], j+1, false);
                    }
                }
            }
        }
        return changed;
    }

    private boolean updateSingletonColumn(SudokuGrid sudoku, int col) {
        int[][] d = new int[9][2];
        boolean[] posibleValues;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            posibleValues = sudoku.getPossibleValues(i, col);
            for (int j = 0; j < posibleValues.length; j++) {
                if (posibleValues[j]) {
                    d[j][0]++;
                    d[j][1] = i;
                }
            }
        }
        boolean changed = false;
        for (int i = 0; i < d.length; i++) {
            if (d[i][0] == 1) {                
                for (int j = 0; j < SudokuGrid.SIZE; j++) {
                    if (j != i) {
                        changed |= sudoku.setPossibleValue(d[i][1], col, j+1, false);
                    }
                }
            }
        }
        return changed;
    }

    private boolean updateSingletonSquare(SudokuGrid sudoku, int row, int col) {
        int[][] d = new int[9][2];
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            boolean[] posibleValues = sudoku.getPossibleValues(row * 3 + i / 3, col * 3 + i % 3);
            for (int j = 0; j < posibleValues.length; j++) {
                if (posibleValues[j]) {
                    d[j][0]++;
                    d[j][1] = i;
                }
            }
        }
        boolean changed = false;
        for (int i = 0; i < d.length; i++) {
            if (d[i][0] == 1) {
                for (int j = 0; j < SudokuGrid.SIZE; j++) {
                    if (j != i) {
                        changed |= sudoku.setPossibleValue(row * 3 + d[i][1] / 3, col * 3 + d[i][1] % 3, j + 1, false);
                    }
                }
            }
        }
        return changed;
    }
    //</editor-fold>
}
