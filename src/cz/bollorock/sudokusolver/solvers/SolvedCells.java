/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.solvers;

import cz.bollorock.sudokusolver.SudokuGrid;

/**
 *
 * @author Ladislav
 */
public class SolvedCells extends Solver {

    public static final String NAME = "Solved Cell";
    public static final int COMPLEXITY = 0;

    public SolvedCells() {
        super(NAME, COMPLEXITY);
    }

    @Override
    public boolean applySolver(SudokuGrid sudoku) {
        boolean valueChanged = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            for (int j = 0; j < SudokuGrid.SIZE; j++) {
                valueChanged = valueChanged | sudoku.updateValue(i, j);
            }
        }
        return valueChanged;
    }
}
