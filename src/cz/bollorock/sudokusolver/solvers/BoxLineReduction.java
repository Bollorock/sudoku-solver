/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.solvers;

import cz.bollorock.sudokusolver.SudokuGrid;
import java.util.Arrays;

/**
 *
 * @author Ladislav
 */
public class BoxLineReduction extends Solver {

    public static final String NAME = "Box-Line Reduction";
    public static final int COMPLEXITY = 5;

    public BoxLineReduction() {
        super(NAME, COMPLEXITY);
    }

    @Override
    public boolean applySolver(SudokuGrid sudoku) {
        boolean change = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            change |= findBoxLineReductionRow(sudoku, i);
            change |= findBoxLineReductionCol(sudoku, i);
        }
        return change;
    }

    private boolean findBoxLineReductionRow(SudokuGrid s, int row) {
        int[] points = new int[SudokuGrid.SIZE];
        Arrays.fill(points, -1);
        for (int c = 0; c < SudokuGrid.SIZE; c++) {
            boolean[] possibleValues = s.getPossibleValues(row, c);
            for (int j = 0; j < SudokuGrid.SIZE; j++) {
                if (!possibleValues[j]) {
                    continue;
                }
                if (points[j] == -1 || points[j] == c / 3) {
                    points[j] = c / 3;
                } else if (points[j] >= 0 && points[j] != c / 3) {
                    points[j] = -2;
                }
            }
        }

        boolean change = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            if (points[i] >= 0) {
                for (int j = 0; j < SudokuGrid.SIZE; j++) {
                    if ((row/3)*3+ j / 3 != row) {
                        int r = (row/3)*3 + j /3;
                        int c = points[i]*3 + j % 3;
                        change |= s.setPossibleValue(r, c, i + 1, false);
                    }
                }
            }
        }

        return change;
    }

    private boolean findBoxLineReductionCol(SudokuGrid s, int col) {
        int[] points = new int[SudokuGrid.SIZE];
        Arrays.fill(points, -1);
        for (int r = 0; r < SudokuGrid.SIZE; r++) {
            boolean[] possibleValues = s.getPossibleValues(r, col);
            for (int j = 0; j < SudokuGrid.SIZE; j++) {
                if (!possibleValues[j]) {
                    continue;
                }
                if (points[j] == -1 || points[j] == r / 3) {
                    points[j] = r / 3;
                } else if (points[j] >= 0 && points[j] != r / 3) {
                    points[j] = -2;
                }
            }
        }

        boolean change = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            if (points[i] >= 0) {
                for (int j = 0; j < SudokuGrid.SIZE; j++) {
                    if ((col/3)*3+ j / 3 != col) {
                        int r = points[i]*3 + j % 3;
                        int c = (col/3)*3 + j /3;
                        change |= s.setPossibleValue(r, c, i + 1, false);
                    }
                }
            }
        }

        return change;
    }
}
