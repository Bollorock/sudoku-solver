package cz.bollorock.sudokusolver.solvers;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import cz.bollorock.sudokusolver.SudokuGrid;

/**
 *
 * @author Ladislav
 */
public abstract class Solver implements Comparable<Solver> {

    private final String name;
    private final int complexity; 

    public Solver(String name, int complexity) {
        this.name = name;
        this.complexity = complexity;
    }

    public String getName() {
        return name;
    }

    public int getComplexity() {
        return complexity;
    }

    @Override
    public String toString() {
        return name + " (" + complexity + ')';
    }

    @Override
    public int compareTo(Solver other) {
        int i = this.getComplexity() - other.getComplexity();
        if (i == 0) {
            return this.getName().compareTo(other.getName());
        }
        return i;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.name.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Solver other = (Solver) obj;
        if (!this.name.equals(other.name)) {
            return false;
        }
        return true;
    }
    
    /**
     * Apply algorithm to given sudoku. Solver can't change number of solutions.
     * @param sudoku Sudoku to solve
     * @return True if change has been made to the sudoku
     */
    public abstract boolean applySolver(SudokuGrid sudoku);
}
