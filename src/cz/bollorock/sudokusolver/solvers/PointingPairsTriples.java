/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.solvers;

import cz.bollorock.sudokusolver.SudokuGrid;
import java.util.Arrays;

/**
 *
 * @author Ladislav
 */
public class PointingPairsTriples extends Solver {

    public static final String NAME = "Pointing Pairs/Triples";
    public static final int COMPLEXITY = 4;

    public PointingPairsTriples() {
        super(NAME, COMPLEXITY);
    }

    @Override
    public boolean applySolver(SudokuGrid sudoku) {
        boolean change = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            change |= findGroupsSquare(sudoku, i / 3, i % 3);
        }
        return change;
    }

    private boolean findGroupsSquare(SudokuGrid s, int row, int col) {
        int[][] points = new int[SudokuGrid.SIZE][2];
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            Arrays.fill(points[i], -1);
        }
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            boolean[] possibleValues = s.getPossibleValues(row * 3 + i / 3, col * 3 + i % 3);
            for (int j = 0; j < SudokuGrid.SIZE; j++) {
                if (!possibleValues[j]) {
                    continue;
                }
                if (points[j][0] == -1 || points[j][0] == i / 3) {
                    points[j][0] = i / 3;
                } else if (points[j][0] >= 0 && points[j][0] != i / 3) {
                    points[j][0] = -2;
                }
                if (points[j][1] == -1 || points[j][1] == i % 3) {
                    points[j][1] = i % 3;
                } else if (points[j][1] >= 0 && points[j][1] != i % 3) {
                    points[j][1] = -2;
                }
            }
        }

        boolean change = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            if (points[i][0] >= 0) {
                for (int c = 0; c < SudokuGrid.SIZE; c++) {
                    if (c / 3 != col) {
                        int r = row * 3 + points[i][0];
                        change |= s.setPossibleValue(r, c, i + 1, false);
                    }
                }
            }
            if (points[i][1] >= 0) {
                for (int r = 0; r < SudokuGrid.SIZE; r++) {
                    if (r / 3 != row) {
                        int c = col * 3 + points[i][1];
                        change |= s.setPossibleValue(r, c, i + 1, false);
                    }
                }
            }
        }

        return change;
    }
}
