/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.solvers;

import cz.bollorock.sudokusolver.SudokuGrid;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ladislav
 */
public class NakedPairs extends Solver {

    public static final String NAME = "Naked Pairs";
    public static final int COMPLEXITY = 3;

    public NakedPairs() {
        super(NAME, COMPLEXITY);
    }

    @Override
    public boolean applySolver(SudokuGrid sudoku) {
        boolean changed = false;
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            changed |= findNakedPairsRow(sudoku, i);
            changed |= findNakedPairsColumn(sudoku, i);
            changed |= findNakedPairsSquare(sudoku, i / 3, i % 3);
        }
        return changed;
    }

    private boolean findNakedPairsRow(SudokuGrid s, int row) {
        HashMap<Pair, Pair> pairs = new HashMap<>();
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            if (s.getPossibleValuesCount(row, i) == 2) {
                int val1 = -1;
                int val2 = -1;
                boolean[] posibleValues = s.getPossibleValues(row, i);
                for (int j = 0; j < SudokuGrid.SIZE && (val1 < 0 || val2 < 0); j++) {
                    if (posibleValues[j] && val1 == -1) {
                        val1 = j + 1;
                    } else if (posibleValues[j] && val1 != -1) {
                        val2 = j + 1;
                    }
                }
                if (val1 > 0 && val2 > 0) {
                    Pair p = new Pair(val1, val2);
                    if (pairs.containsKey(p)) {
                        pairs.get(p).val2 = i;
                    } else {
                        pairs.put(p, new Pair(i, -1));
                    }
                }
            }
        }
        boolean changed = false;
        for (Map.Entry<Pair, Pair> entry : pairs.entrySet()) {
            if (entry.getValue().val2 != -1) {
                for (int i = 0; i < SudokuGrid.SIZE; i++) {
                    if (i != entry.getValue().val1 && i != entry.getValue().val2) {
                        changed |= s.setPossibleValue(row, i, entry.getKey().val1, false);
                        changed |= s.setPossibleValue(row, i, entry.getKey().val2, false);
                    }
                }
            }
        }
        return changed;
    }

    private boolean findNakedPairsColumn(SudokuGrid s, int col) {
        HashMap<Pair, Pair> pairs = new HashMap<>();
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            if (s.getPossibleValuesCount(i, col) == 2) {
                int val1 = -1;
                int val2 = -1;
                boolean[] posibleValues = s.getPossibleValues(i, col);
                for (int j = 0; j < SudokuGrid.SIZE && (val1 < 0 || val2 < 0); j++) {
                    if (posibleValues[j] && val1 == -1) {
                        val1 = j + 1;
                    } else if (posibleValues[j] && val1 != -1) {
                        val2 = j + 1;
                    }
                }
                if (val1 > 0 && val2 > 0) {
                    Pair p = new Pair(val1, val2);
                    if (pairs.containsKey(p)) {
                        pairs.get(p).val2 = i;
                    } else {
                        pairs.put(p, new Pair(i, -1));
                    }
                }
            }
        }
        boolean changed = false;
        for (Map.Entry<Pair, Pair> entry : pairs.entrySet()) {
            if (entry.getValue().val2 != -1) {
                for (int i = 0; i < SudokuGrid.SIZE; i++) {
                    if (i != entry.getValue().val1 && i != entry.getValue().val2) {
                        changed |= s.setPossibleValue(i, col, entry.getKey().val1, false);
                        changed |= s.setPossibleValue(i, col, entry.getKey().val2, false);
                    }
                }
            }
        }
        return changed;
    }

    private boolean findNakedPairsSquare(SudokuGrid s, int row, int col) {
        HashMap<Pair, Pair> pairs = new HashMap<>();
        for (int i = 0; i < SudokuGrid.SIZE; i++) {
            if (s.getPossibleValuesCount(row * 3 + i / 3, col * 3 + i % 3) == 2) {
                int val1 = -1;
                int val2 = -1;
                boolean[] posibleValues = s.getPossibleValues(row * 3 + i / 3, col * 3 + i % 3);
                for (int j = 0; j < SudokuGrid.SIZE && (val1 < 0 || val2 < 0); j++) {
                    if (posibleValues[j] && val1 == -1) {
                        val1 = j + 1;
                    } else if (posibleValues[j] && val1 != -1) {
                        val2 = j + 1;
                    }
                }
                if (val1 > 0 && val2 > 0) {
                    Pair p = new Pair(val1, val2);
                    if (pairs.containsKey(p)) {
                        pairs.get(p).val2 = i;
                    } else {
                        pairs.put(p, new Pair(i, -1));
                    }
                }
            }
        }
        boolean changed = false;
        for (Map.Entry<Pair, Pair> entry : pairs.entrySet()) {
            if (entry.getValue().val2 != -1) {
                for (int i = 0; i < SudokuGrid.SIZE; i++) {
                    if (i != entry.getValue().val1 && i != entry.getValue().val2) {
                        changed |= s.setPossibleValue(row * 3 + i / 3, col * 3 + i % 3, entry.getKey().val1, false);
                        changed |= s.setPossibleValue(row * 3 + i / 3, col * 3 + i % 3, entry.getKey().val2, false);
                    }
                }
            }
        }
        return changed;
    }

    private class Pair {

        int val1, val2;

        public Pair(int val1, int val2) {
            this.val1 = val1;
            this.val2 = val2;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 19 * hash + this.val1;
            hash = 19 * hash + this.val2;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Pair other = (Pair) obj;
            if (this.val1 != other.val1) {
                return false;
            }
            if (this.val2 != other.val2) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "Pair{" + "val1=" + val1 + ", val2=" + val2 + '}';
        }
    }
}
