/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver;

/**
 *
 * @author Ladislav Mejzlík
 */
public class Field {

    private int value; // Value of the field, non-positive value means empty field
    private boolean[] possibleValues;
    private int possibleValuesCount;
    private boolean initial; // Determines if the field is part of the task

    /**
     * Creates Field and sets initial flag if value is greater than 0.
     *
     * @param value Digit from 0 to 9, 0 means empty field.
     */
    public Field(int value) {
        if (value < 0 || value > 9) {
            throw new IllegalArgumentException();
        }
        this.value = value;
        initial = value > 0;
        possibleValuesCount = initial ? 0 : 9;
        possibleValues = new boolean[9];
        for (int i = 0; i < possibleValues.length; i++) {
            possibleValues[i] = !initial;
        }
    }

    public boolean isInitial() {
        return initial;
    }

    public void setInitial(boolean initial) {
        this.initial = initial;
    }

    /**
     * Updates initial flag.
     *
     * If value is greater than 0 sets initial to true. Otherwise sets initial
     * to false.
     */
    public void updateInitial() {
        initial = value > 0;
    }

    /**
     * Updates field value.
     *
     * If there is only one possible value, writes it in the field.
     *
     * @return If the value has been changed
     */
    public boolean updateValue() {
        if (value > 0 || possibleValuesCount != 1) {
            return false;
        }
        for (int i = 0; i < possibleValues.length; i++) {
            if (possibleValues[i]) {
                this.setValue(i + 1);
                return true;
            }
        }
        return false;
    }

    public int getValue() {
        return value;
    }

    /**
     * Set field value and updates possible values.
     * @param value
     * @return If the value has been changed
     */
    public boolean setValue(int value) {
        if (value < 0 || value > 9) {
            throw new IllegalArgumentException();
        }
        boolean changed = this.value != value;
        this.value = value;
        boolean b = value <= 0;
        for (int i = 0; i < possibleValues.length; i++) {
            possibleValues[i] = b;
        }
        possibleValuesCount = b ? 9 : 0;
        return changed;
    }

    public boolean[] getPosibleValues() {
        return possibleValues;
    }

    public int getPosibleValuesCount() {
        return possibleValuesCount;
    }

    public void setPosibleValues(boolean[] posibleValues) {
        this.possibleValues = posibleValues;
    }

    /**
     * Set if specified value is possible.
     * 
     * @param value
     * @param posible
     * @return If possible values has been changed.
     */
    public boolean setPosibleValue(int value, boolean posible) {
        if (this.value > 0 || value <= 0 || value > 9) {
            return false;
        }
        boolean result = possibleValues[value - 1] != posible;
        possibleValues[value - 1] = posible;
        if (result) {
            possibleValuesCount = posible ? possibleValuesCount + 1 : possibleValuesCount - 1;
        }
        return result;
    }

    public boolean removePosibleValues(boolean[] remove) {
        boolean changed = false;
        for (int i = 0; i < possibleValues.length; i++) {
            changed |= possibleValues[i] && remove[i];
            possibleValuesCount = possibleValues[i] && remove[i] ? possibleValuesCount - 1 : possibleValuesCount;
            possibleValues[i] &= !remove[i];
        }
        return changed;
    }

    public boolean isPosible(int value) {
        return value >= 1 && value <= 9 && possibleValues[value - 1];
    }

    @Override
    public String toString() {
        return value + "";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.value;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Field other = (Field) obj;
        if (this.value != other.value) {
            return false;
        }
        return true;
    }
}
