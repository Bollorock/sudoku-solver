/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.io;

import cz.bollorock.sudokusolver.SudokuGrid;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ladislav
 */
public class FileSS implements FileType {
    
    public static final String EXTENSION = "ss";
    public static final String NAME = "SimpleSudoku";
    public static final String DESC = "Format used by program SimpleSudoku (angusj.com/sudoku).";
    
    @Override
    public String getDescription() {
        return DESC;
    }
    
    @Override
    public String getFileExtension() {
        return EXTENSION;
    }
    
    @Override
    public String getName() {
        return NAME;
    }
    
    @Override
    public SudokuGrid loadSudoku(File file) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {
            }
        }
        String s = sb.toString().trim().replaceAll("[-|+*]", "");
        if (s.length() > SudokuGrid.SIZE * SudokuGrid.SIZE) {
            s = s.substring(0, SudokuGrid.SIZE*SudokuGrid.SIZE);
        }
        if (s.matches(SudokuGrid.VALIDATION_REGEXP)) {
            return new SudokuGrid(s);
        } else {
            throw new IOException("File \"" + file.getAbsolutePath() + "\" is corrupted.");
        }
    }

    @Override
    public void saveSudoku(SudokuGrid sudoku, File file) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
