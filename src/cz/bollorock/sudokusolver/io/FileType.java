/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.io;

import cz.bollorock.sudokusolver.SudokuGrid;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Ladislav Mejzlík
 */
public interface FileType {

    /**
     * Returns description of the file type
     */
    public String getDescription();

    /**
     * Returns file extension (without dot, e.g. "java" or "class")
     */
    public String getFileExtension();

    /**
     * Returns name of the file type
     */
    public String getName();

    /**
     * Loads sudoku from given file
     *
     * @param file File to read from
     * @return Loaded sudoku
     * @throws IOException
     */
    public SudokuGrid loadSudoku(File file) throws IOException;

    /**
     * Saves sudoku to given file
     *
     * @param file File to write to. File doesn't have to exist, will overwrite existing file. 
     * @throws IOException
     */
    public void saveSudoku(SudokuGrid sudoku, File file) throws IOException;
}
