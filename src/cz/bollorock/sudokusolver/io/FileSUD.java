/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.io;

import cz.bollorock.sudokusolver.SudokuGrid;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Ladislav
 */
public class FileSUD implements FileType {

    public static final String DESC = "File format used by Bollorock's Sudoku Solver. Supports saving and loading possible values.";
    public static final String EXT = "sud";
    public static final String NAME = "Sudoku Solver";

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public String getFileExtension() {
        return EXT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public SudokuGrid loadSudoku(File file) throws IOException{
        SudokuGrid sudoku = new SudokuGrid();
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            for (int r = 0; r < SudokuGrid.SIZE; r++) {
                for (int c = 0; c < SudokuGrid.SIZE; c++) {
                    sudoku.setValue(r, c, in.read());
                    sudoku.setInitial(r, c, in.read()==1);
                    sudoku.setPossibleValues(r, c, getBooleanArray(in.read(), SudokuGrid.SIZE));
                }
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
            }
        }
        return sudoku;
    }

    @Override
    public void saveSudoku(SudokuGrid sudoku, File file) throws IOException {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            for (int r = 0; r < SudokuGrid.SIZE; r++) {
                for (int c = 0; c < SudokuGrid.SIZE; c++) {
                    out.write(sudoku.getValue(r, c));
                    out.write(sudoku.isInitial(r, c) ? 1 : 0);
                    out.write(getIntValue(sudoku.getPossibleValues(r, c)));
                }
            }
            out.flush();
        } catch (IOException ex) {
            throw ex;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
            }
        }
    }

    private static int getIntValue(boolean[] array) {
        int res = 0;
        int digit = 1;
        for (int i = 0; i < array.length; i++) {
            if (array[i]) {
                res += digit;
            }
            digit <<= 1;
        }
        return res;
    }

    private static boolean[] getBooleanArray(int integer, int minDigits) {
        int size = (int) Math.max(Math.floor(Math.log(integer) / Math.log(2)) + 1, minDigits);
        boolean[] res = new boolean[size];
        int digit = 1;
        for (int i = 0; i < size; i++) {
            res[i] = (integer & digit) == digit;
            digit <<= 1;
        }
        return res;
    }
}
