/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver.io;

import cz.bollorock.sudokusolver.SudokuGrid;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Ladislav Mejzlík
 */
public class SudokuIO {

    private HashMap<String, FileType> supportedFileTypes;

    public SudokuIO() {
        supportedFileTypes = new HashMap<>();
        supportedFileTypes.put(FileSS.EXTENSION, new FileSS());
        supportedFileTypes.put(FileSUD.EXT, new FileSUD());
    }

    /**
     * Open GUI for selecting file and reads sudoku from the file.
     *
     * @param parrent Parrent GUI for modality
     * @return Loaded sudoku or null if user canceled selection
     * @throws IOException
     */
    public SudokuGrid openSudoku(Component parrent) throws IOException {
        JFileChooser fc = new JFileChooser("Sudokus");
        for (FileType fileType : supportedFileTypes.values()) {
            fc.addChoosableFileFilter(new FileNameExtensionFilter(fileType.getName(), fileType.getFileExtension()));
        }
        int showOpenDialog = fc.showOpenDialog(parrent);
        if (showOpenDialog == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            return readSudoku(f);
        }
        return null;
    }

    /**
     * Open GUI for selecting file and writes sudoku to the file.
     *
     * @param parrent Parrent GUI for modality
     * @return Loaded sudoku or null if user canceled selection
     * @throws IOException
     */
    public boolean saveSudoku(Component parrent, SudokuGrid sudoku) throws IOException {
        JFileChooser fc = new JFileChooser("Sudokus");
        for (FileType fileType : supportedFileTypes.values()) {
            fc.addChoosableFileFilter(new FileNameExtensionFilter(fileType.getName(), fileType.getFileExtension()));
        }
        int showSaveDialog = fc.showSaveDialog(parrent);
        if (showSaveDialog == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            writeSudoku(f, sudoku);
            return true;
        }
        return false;

    }

    /**
     * Reads file using supported file types (FileType)
     *
     * @param f File to read from
     * @return Sudoku or null if file type of the given type is not supported
     * @throws IOException
     */
    public SudokuGrid readSudoku(File f) throws IOException {
        if (f == null || !f.isFile() || !f.canRead()) {
            return null;
        }
        String s = f.getName();
        s = s.substring(s.lastIndexOf('.') + 1);
        FileType ft = supportedFileTypes.get(s);
        return ft != null ? ft.loadSudoku(f) : null;
    }

    /**
     * Writes sudoku using supported file types (FileType)
     *
     * @param f File to write to
     * @param s Sudoku to write to file
     * @return False if file type of the given type is not supported, else true
     * @throws IOException
     */
    public boolean writeSudoku(File f, SudokuGrid s) throws IOException {
        if (f == null || s == null) {
            throw new IOException("Can't write to file: " + f);
        }
        String ext = f.getName();
        ext = ext.substring(ext.lastIndexOf('.') + 1);
        FileType ft = supportedFileTypes.get(ext);
        if (ft == null) {
            return false;
        }
        ft.saveSudoku(s, f);
        return true;
    }
}
