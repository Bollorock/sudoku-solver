/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver;

/**
 *
 * @author Ladislav
 */
public enum SudokuState {

    SOLVED,
    INCOMPLETE,
    ERROR_MULT,
    ERROR_EMPTY;
}
