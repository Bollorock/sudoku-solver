/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver;

/**
 *
 * @author Ladislav
 */
public enum StopReason {

    PROPPER,
    ABORTED,
    TIMEOUT,
    SOLUTIONS_FOUND;
}
