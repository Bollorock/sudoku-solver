/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.bollorock.sudokusolver;

import cz.bollorock.sudokusolver.solvers.RegisteredSolvers;
import cz.bollorock.sudokusolver.solvers.Solver;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author Ladislav
 */
public class SudokuGrid {

    public static final int SIZE = 9;
    public static final int SMALL_SIZE = 3;
    private static final String EMPTY_CHARS = ",-._Xx0Oo";
    public static final String VALIDATION_REGEXP = "[0-9" + EMPTY_CHARS + "]{" + SIZE * SIZE + "}";
    private Field[][] grid;
    private volatile boolean solving = false;

    //<editor-fold defaultstate="collapsed" desc="Constructors">
    public SudokuGrid() {
        grid = new Field[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                grid[i][j] = new Field(0);
            }
        }
    }

    public SudokuGrid(int[][] initial) {
        grid = new Field[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                grid[i][j] = new Field(initial[i][j]);
            }
        }
    }

    public SudokuGrid(SudokuGrid grid, boolean coppyPossibleValues) {
        this();
        this.setValues(grid, coppyPossibleValues);
    }

    public SudokuGrid(String initial) {
        this();
        this.setValues(initial);
    }
    //</editor-fold>

    public int getFilledCount() {
        int count = 0;
        for (int r = 0; r < SIZE; r++) {
            for (int c = 0; c < SIZE; c++) {
                if (grid[r][c].getValue() > 0) {
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public String toString() {
        String s = "+---+---+---+\n";
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < SIZE; i++) {
            sb.append("|");
            for (int j = 0; j < SIZE; j++) {
                sb.append(grid[i][j]);
                if (j % 3 == 2) {
                    sb.append("|");
                }
            }
            sb.append("\n");
            if (i % 3 == 2) {
                sb.append(s);
            }
        }
        return sb.toString().trim();
    }

    public String toLineString() {
        String s = "";
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                s += grid[i][j].getValue();
            }
        }
        return s;
    }

    public boolean setValue(int row, int col, int val) {
        return grid[row][col].setValue(val);
    }

    public int getValue(int row, int col) {
        return grid[row][col].getValue();
    }

    public boolean setPossibleValue(int row, int col, int val, boolean pos) {
        return grid[row][col].setPosibleValue(val, pos);
    }

    public void setPossibleValues(int row, int col, boolean[] possibleValues) {
        grid[row][col].setPosibleValues(possibleValues);
    }

    public boolean isSolved() {
        return getState() == SudokuState.SOLVED;
    }

    public SudokuState getState() {
        SudokuState state = SudokuState.SOLVED;
        for (int i = 0; i < SIZE; i++) {
            SudokuState s;
            s = this.getStateCol(i);
            if (s == SudokuState.ERROR_EMPTY || s == SudokuState.ERROR_MULT) {
                state = s;
                break;
            } else if (s == SudokuState.INCOMPLETE) {
                state = SudokuState.INCOMPLETE;
            }

            s = this.getStateRow(i);
            if (s == SudokuState.ERROR_EMPTY || s == SudokuState.ERROR_MULT) {
                state = s;
                break;
            } else if (s == SudokuState.INCOMPLETE) {
                state = SudokuState.INCOMPLETE;
            }

            s = this.getStateSquare(i / 3, i % 3);
            if (s == SudokuState.ERROR_EMPTY || s == SudokuState.ERROR_MULT) {
                state = s;
                break;
            } else if (s == SudokuState.INCOMPLETE) {
                state = SudokuState.INCOMPLETE;
            }
        }
        return state;
    }

    public void stopSolving() {
        solving = false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + this.toLineString().hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SudokuGrid other = (SudokuGrid) obj;

        return this.toLineString().equals(other.toLineString());
    }

    //<editor-fold defaultstate="collapsed" desc="getState">
    private SudokuState getStateRow(int row) {
        boolean[] numbers = new boolean[SIZE];
        boolean complete = true;
        int value;
        for (int i = 0; i < SIZE; i++) {
            value = grid[row][i].getValue();
            complete &= value > 0;
            if (value > 0) {
                if (numbers[value - 1]) {
                    return SudokuState.ERROR_MULT;
                }
                numbers[value - 1] = true;
            } else {
                boolean[] posibleValues = grid[row][i].getPosibleValues();
                boolean b = false;
                for (int j = 0; j < posibleValues.length && !b; j++) {
                    b |= posibleValues[j];
                }
                if (!b) {
                    return SudokuState.ERROR_EMPTY;
                }
            }
        }
        return complete ? SudokuState.SOLVED : SudokuState.INCOMPLETE;
    }

    private SudokuState getStateCol(int col) {
        boolean[] numbers = new boolean[SIZE];
        boolean complete = true;
        int value;
        for (int i = 0; i < SIZE; i++) {
            value = grid[i][col].getValue();
            complete &= value > 0;
            if (value > 0) {
                if (numbers[value - 1]) {
                    return SudokuState.ERROR_MULT;
                }
                numbers[value - 1] = true;
            } else {
                boolean[] posibleValues = grid[i][col].getPosibleValues();
                boolean b = false;
                for (int j = 0; j < posibleValues.length && !b; j++) {
                    b |= posibleValues[j];
                }
                if (!b) {
                    return SudokuState.ERROR_EMPTY;
                }
            }
        }
        return complete ? SudokuState.SOLVED : SudokuState.INCOMPLETE;
    }

    private SudokuState getStateSquare(int row, int col) {
        boolean[] numbers = new boolean[SIZE];
        boolean complete = true;
        int value;
        for (int i = 0; i < SIZE; i++) {
            value = grid[row * SMALL_SIZE + i / SMALL_SIZE][col * SMALL_SIZE + i % SMALL_SIZE].getValue();
            complete &= value > 0;
            if (value > 0) {
                if (numbers[value - 1]) {
                    return SudokuState.ERROR_MULT;
                }
                numbers[value - 1] = true;
            } else {
                boolean[] posibleValues = grid[row * SMALL_SIZE + i / SMALL_SIZE][col * SMALL_SIZE + i % SMALL_SIZE].getPosibleValues();
                boolean b = false;
                for (int j = 0; j < posibleValues.length && !b; j++) {
                    b |= posibleValues[j];
                }
                if (!b) {
                    return SudokuState.ERROR_EMPTY;
                }
            }
        }
        return complete ? SudokuState.SOLVED : SudokuState.INCOMPLETE;
    }
    //</editor-fold>

    public final void setValues(String newSudoku) {
        if (newSudoku == null || !newSudoku.matches(VALIDATION_REGEXP)) {
            throw new IllegalArgumentException("Invalid sudoku string.");
        }
        newSudoku = newSudoku.replaceAll("[" + EMPTY_CHARS + "]", "0");
        for (int i = 0; i < SIZE * SIZE; i++) {
            grid[i / SIZE][i % SIZE].setValue(newSudoku.charAt(i) - '0');
            grid[i / SIZE][i % SIZE].updateInitial();
        }
    }

    public final void setValues(SudokuGrid newSudoku, boolean copyPossibleValues) {
        if (newSudoku == null) {
            throw new IllegalArgumentException("New sudoku is null");
        }

        for (int r = 0; r < SIZE; r++) {
            for (int c = 0; c < SIZE; c++) {
                int value = newSudoku.getValue(r, c);
                grid[r][c].setValue(value);
                if (copyPossibleValues && value <= 0) {
                    grid[r][c].setPosibleValues(newSudoku.getPossibleValues(r, c));
                }
                grid[r][c].setInitial(newSudoku.isInitial(r, c));
            }
        }

    }

    public boolean[] getPossibleValues(int row, int col) {
        return grid[row][col].getPosibleValues();
    }

    public void clear() {
        for (int r = 0; r < SIZE; r++) {
            for (int c = 0; c < SIZE; c++) {
                grid[r][c].setValue(0);
            }
        }
    }

    public void reset() {
        for (int r = 0; r < SIZE; r++) {
            for (int c = 0; c < SIZE; c++) {
                if (!grid[r][c].isInitial()) {
                    grid[r][c].setValue(0);
                }
            }
        }
    }

    public boolean updateValue(int row, int col) {
        return grid[row][col].updateValue();
    }

    public int getPossibleValuesCount(int row, int col) {
        return grid[row][col].getPosibleValuesCount();
    }

    public boolean isInitial(int row, int col) {
        return grid[row][col].isInitial();
    }

    public boolean removePosibleValues(int row, int col, boolean[] used) {
        return grid[row][col].removePosibleValues(used);
    }

    /**
     * Tries to solve sudoku using logic methods from RegisteredSolvers.
     * 
     * @return Response with type of solver used and state of sudoku.
     */
    public SolverResponse logicSolve() {
        boolean change = true;
        while (change) {
            change = false;
            Iterator<Map.Entry<Solver, Boolean>> it = RegisteredSolvers.getSolvers().entrySet().iterator();
            while (it.hasNext() && !change) {
                Map.Entry<Solver, Boolean> s = it.next();
                if (s.getValue()) {
                    change |= s.getKey().applySolver(this);
                }
            }
        }
        SudokuState state = getState();
        return new SolverResponse(StopReason.PROPPER, state, true, state == SudokuState.SOLVED ? 1 : 0, "");
    }

    /**
     * Tries to solve sudoku using logical methods and backtracking (guesing).
     * 
     * Solving can be stopped using method stopSolving
     * 
     * @param timeOut Maximal time that solver can run in milliseconds. 0 means disabled
     * @param enoughSolutions Solver will stop solving when it finds enough solutions. 0 means find all solutions
     * @return 
     */
    public SolverResponse backtrackSolve(long timeOut, int enoughSolutions) {
        long t = System.currentTimeMillis();
        solving = true;

        //Trying logic solve first
        SolverResponse logicSolve = this.logicSolve();
        if (logicSolve.getSudokuState() != SudokuState.INCOMPLETE) {
            solving = false;
            return logicSolve;
        }

        //Setting up queues (queue: q; solutions: s; visited: v)
        LinkedList<SudokuGrid> q = new LinkedList<>();
        HashSet<SudokuGrid> s = new HashSet<>();
        HashSet<SudokuGrid> v = new HashSet<>();
        q.add(this);

        StopReason breaked = StopReason.PROPPER;
        while (!q.isEmpty()) {
            SudokuGrid node = q.pollFirst();            
            processNode(node, s, v, q);
            //Cheking if I should continue
            if (enoughSolutions > 0 && s.size() >= enoughSolutions) {
                breaked = StopReason.SOLUTIONS_FOUND;
                break;
            }
            if ((timeOut > 0 && System.currentTimeMillis() - t > timeOut)) {
                breaked = StopReason.TIMEOUT;
                break;
            }
            if (!solving) {
                breaked = StopReason.ABORTED;
                break;
            }
        }

        //Writing solution
        if (!s.isEmpty()) {
            SudokuGrid node = s.iterator().next();
            for (int r = 0; r < SIZE; r++) {
                for (int c = 0; c < SIZE; c++) {
                    this.setValue(r, c, node.getValue(r, c));
                }
            }
        }
        solving = false;
        return new SolverResponse(breaked, this.getState(), false, s.size(), "Backtrack used " + 0 +  " times");
    }

    public void setInitial(int r, int c, boolean initial) {
        grid[r][c].setInitial(initial);
    }

    private void processNode(SudokuGrid node, HashSet<SudokuGrid> s, HashSet<SudokuGrid> v, LinkedList<SudokuGrid> q) {
        //Find first empty cell (room for improvement by choosing cell with fewest possible values?)
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (node.getValue(i, j) <= 0) {
                    //Try to solve every possible value
                    boolean[] posibleValues = node.getPossibleValues(i, j);
                    for (int k = 0; k < posibleValues.length; k++) {
                        if (posibleValues[k]) {
                            SudokuGrid child = new SudokuGrid(node, false);
                            child.setValue(i, j, k + 1);
                            SudokuState state = child.logicSolve().getSudokuState();
                            if (state == SudokuState.SOLVED) {
                                s.add(child);
                            } else if (state == SudokuState.INCOMPLETE && !v.contains(child)) {
                                q.add(child);
                            }
                            v.add(child);
                        }
                    }
                }
            }
        }
    }
}
